import unittest
from app import app

class TestAdd(unittest. TestCase):
    def test_add(self):
        api = app.test_client()
        response = api.get('/add?a=1&b=2')
        result = int(response.data)
        self.assertEqual(3, result)
    def test_sub(self):
        api = app.test_client()
        response = api.get('/sub?a=1&b=2')
        result = int(response.data)
        self.assertEqual(-1, result)
            
if __name__ == '__main__':
    unittest.main()